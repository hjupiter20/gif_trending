import 'package:flutter/material.dart';

///
/// Configuración para hacer uso del tamaño de la pantalla
///
/// [blockSizeVertical]     alto de la pantalla
/// [blockSizeHorizontal]   ancho de la pantalla
///
/// [safeBlockHorizontal]   ancho de la pantalla considerando el area segura
/// [safeBlockVertical]     alto de la pantalla considerando el area segura
///
class SizeConfig {
  static double _safeAreaHorizontal = 0;
  static double _safeAreaVertical = 0;

  /// mediaquery for control
  static MediaQueryData _mediaQueryData = const MediaQueryData();

  /// size of width of screen in percent
  static double screenWidth = 0;

  /// size of height of screen in percent
  static double screenHeight = 0;

  /// size of width of screen in pixels
  static double blockSizeHorizontal = 0;

  /// size of heigth of screen in pixels
  static double blockSizeVertical = 0;

  /// size of width of screen in percent without safe
  static double safeBlockHorizontal = 0;

  /// size of heigth of screen in percent without safe
  static double safeBlockVertical = 0;

  /// init of size config util
  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
  }
}
