import 'package:flutter/material.dart';

// ignore: public_member_api_docs, constant_identifier_names
const MaterialAccentColor COLOR_PRIMARY = Colors.deepOrangeAccent;
// ignore: public_member_api_docs, constant_identifier_names
const MaterialColor COLOR_ACCENT = Colors.orange;

/// the color ligth
ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  primaryColor: COLOR_PRIMARY,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
        const EdgeInsets.symmetric(
          horizontal: 40.0,
          vertical: 20.0,
        ),
      ),
      shape: MaterialStateProperty.all<OutlinedBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            20.0,
          ),
        ),
      ),
      backgroundColor: MaterialStateProperty.all<Color>(
        COLOR_ACCENT,
      ),
    ),
  ),
  inputDecorationTheme: InputDecorationTheme(
    contentPadding: const EdgeInsets.symmetric(
      vertical: 15,
      horizontal: 20,
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(
        20.0,
      ),
    ),
    fillColor: Colors.grey.withOpacity(
      0.1,
    ),
  ),
);
