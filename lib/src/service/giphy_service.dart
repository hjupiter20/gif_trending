import 'package:built_collection/built_collection.dart';
import 'package:dio/dio.dart';
import 'package:gif_trending/src/model/serializers.dart';
import 'package:gif_trending/src/model/trending/gif.dart';
import 'package:gif_trending/util/environment.dart';

/// service for api
class GiphyService {
  /// get all trending gif
  Future<List<Gif>> getTrending() async {
    Dio dio = Dio();
    try {
      dynamic result = await dio.get(
        Environment.apiUrl +
            Environment.trending +
            '?api_key=' +
            Environment.apiKey,
      );

      BuiltList<Gif> listResp = deserializeListOf<Gif>((result.data['data']));
      return listResp.toList();
    } on DioError catch (err) {
      return <Gif>[];
    }
  }

  /// get all trending gif
  Future<List<Gif>> searchTrending(String value) async {
    Dio dio = Dio();
    value = value.replaceAll(' ', '+');
    try {
      dynamic result = await dio.get(
        Environment.apiUrl +
            Environment.gif +
            '/search?api_key=' +
            Environment.apiKey +
            '&q=' +
            value +
            '&limit=50'+
            '&offset=5&rating=g&lang=es'
      );

      BuiltList<Gif> listResp = deserializeListOf<Gif>((result.data['data']));
      return listResp.toList();
    } on DioError catch (err) {
      return <Gif>[];
    }
  }
}
