import 'package:get/get.dart';
import 'package:gif_trending/src/model/trending/gif.dart';
import 'package:gif_trending/src/service/giphy_service.dart';

/// controller for home_page
class HomeController  extends GetxController {


  /// service to consul
  final GiphyService _giphyService = GiphyService();

  /// list of all trending
  final RxList<Gif> lstItems = <Gif>[].obs;

  /// method for get all trending
  getItems() async {
    lstItems.value =  await _giphyService.getTrending();
  }

  ///
  search(String value) async {
    lstItems.value =  await _giphyService.searchTrending(value);
  }

  @override
  void onInit() {
    super.onInit();
    _giphyService.getTrending().then(
      (List<Gif> data )  {
        lstItems.value = data;
      } 
    );
  }

  @override
  void onReady() {}

  @override
  void onClose() {}
}