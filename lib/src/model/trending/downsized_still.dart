import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:gif_trending/src/model/serializers.dart';

part 'downsized_still.g.dart';

///
abstract class DownsizedStill
    implements Built<DownsizedStill, DownsizedStillBuilder> {

      
  ///
  String? get url;

  ///
  String? get width;

  ///
  String? get height;

  DownsizedStill._();

  ///
  factory DownsizedStill([void Function(DownsizedStillBuilder) updates]) =
      _$DownsizedStill;

  ///
  Map<String, dynamic> toJson() {
    return serializers.serializeWith(DownsizedStill.serializer, this)
        as Map<String, dynamic>;
  }

  ///
  static DownsizedStill? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(DownsizedStill.serializer, json);
  }

  ///
  static Serializer<DownsizedStill> get serializer =>
      _$downsizedStillSerializer;
}
