// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gif.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Gif> _$gifSerializer = new _$GifSerializer();

class _$GifSerializer implements StructuredSerializer<Gif> {
  @override
  final Iterable<Type> types = const [Gif, _$Gif];
  @override
  final String wireName = 'Gif';

  @override
  Iterable<Object?> serialize(Serializers serializers, Gif object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
      'images',
      serializers.serialize(object.images,
          specifiedType: const FullType(Images)),
    ];

    return result;
  }

  @override
  Gif deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GifBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'images':
          result.images.replace(serializers.deserialize(value,
              specifiedType: const FullType(Images))! as Images);
          break;
      }
    }

    return result.build();
  }
}

class _$Gif extends Gif {
  @override
  final String id;
  @override
  final String type;
  @override
  final Images images;

  factory _$Gif([void Function(GifBuilder)? updates]) =>
      (new GifBuilder()..update(updates)).build();

  _$Gif._({required this.id, required this.type, required this.images})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'Gif', 'id');
    BuiltValueNullFieldError.checkNotNull(type, 'Gif', 'type');
    BuiltValueNullFieldError.checkNotNull(images, 'Gif', 'images');
  }

  @override
  Gif rebuild(void Function(GifBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GifBuilder toBuilder() => new GifBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Gif &&
        id == other.id &&
        type == other.type &&
        images == other.images;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), type.hashCode), images.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Gif')
          ..add('id', id)
          ..add('type', type)
          ..add('images', images))
        .toString();
  }
}

class GifBuilder implements Builder<Gif, GifBuilder> {
  _$Gif? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _type;
  String? get type => _$this._type;
  set type(String? type) => _$this._type = type;

  ImagesBuilder? _images;
  ImagesBuilder get images => _$this._images ??= new ImagesBuilder();
  set images(ImagesBuilder? images) => _$this._images = images;

  GifBuilder();

  GifBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _type = $v.type;
      _images = $v.images.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Gif other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Gif;
  }

  @override
  void update(void Function(GifBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Gif build() {
    _$Gif _$result;
    try {
      _$result = _$v ??
          new _$Gif._(
              id: BuiltValueNullFieldError.checkNotNull(id, 'Gif', 'id'),
              type: BuiltValueNullFieldError.checkNotNull(type, 'Gif', 'type'),
              images: images.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'images';
        images.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Gif', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
