import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:gif_trending/src/model/serializers.dart';
import 'package:gif_trending/src/model/trending/downsized_still.dart';
import 'package:gif_trending/src/model/trending/original.dart';

part 'images.g.dart';

abstract class Images implements Built<Images, ImagesBuilder> {

  ///
  Original? get original;

  ///
  DownsizedStill? get downsizedStill;

  Images._();

  ///
  factory Images([void Function(ImagesBuilder) updates]) = _$Images;

  ///
  Map<String, dynamic> toJson() {
    return serializers.serializeWith(Images.serializer, this)
        as Map<String, dynamic>;
  }

  ///
  static Images? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(Images.serializer, json);
  }

  ///
  static Serializer<Images> get serializer => _$imagesSerializer;
}
