// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'original.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Original> _$originalSerializer = new _$OriginalSerializer();

class _$OriginalSerializer implements StructuredSerializer<Original> {
  @override
  final Iterable<Type> types = const [Original, _$Original];
  @override
  final String wireName = 'Original';

  @override
  Iterable<Object?> serialize(Serializers serializers, Original object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.url;
    if (value != null) {
      result
        ..add('url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.width;
    if (value != null) {
      result
        ..add('width')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.height;
    if (value != null) {
      result
        ..add('height')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Original deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new OriginalBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'width':
          result.width = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'height':
          result.height = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$Original extends Original {
  @override
  final String? url;
  @override
  final String? width;
  @override
  final String? height;

  factory _$Original([void Function(OriginalBuilder)? updates]) =>
      (new OriginalBuilder()..update(updates)).build();

  _$Original._({this.url, this.width, this.height}) : super._();

  @override
  Original rebuild(void Function(OriginalBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  OriginalBuilder toBuilder() => new OriginalBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Original &&
        url == other.url &&
        width == other.width &&
        height == other.height;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, url.hashCode), width.hashCode), height.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Original')
          ..add('url', url)
          ..add('width', width)
          ..add('height', height))
        .toString();
  }
}

class OriginalBuilder implements Builder<Original, OriginalBuilder> {
  _$Original? _$v;

  String? _url;
  String? get url => _$this._url;
  set url(String? url) => _$this._url = url;

  String? _width;
  String? get width => _$this._width;
  set width(String? width) => _$this._width = width;

  String? _height;
  String? get height => _$this._height;
  set height(String? height) => _$this._height = height;

  OriginalBuilder();

  OriginalBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _url = $v.url;
      _width = $v.width;
      _height = $v.height;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Original other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Original;
  }

  @override
  void update(void Function(OriginalBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Original build() {
    final _$result =
        _$v ?? new _$Original._(url: url, width: width, height: height);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
