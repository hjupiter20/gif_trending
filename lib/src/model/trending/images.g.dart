// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Images> _$imagesSerializer = new _$ImagesSerializer();

class _$ImagesSerializer implements StructuredSerializer<Images> {
  @override
  final Iterable<Type> types = const [Images, _$Images];
  @override
  final String wireName = 'Images';

  @override
  Iterable<Object?> serialize(Serializers serializers, Images object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.original;
    if (value != null) {
      result
        ..add('original')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Original)));
    }
    value = object.downsizedStill;
    if (value != null) {
      result
        ..add('downsizedStill')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DownsizedStill)));
    }
    return result;
  }

  @override
  Images deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ImagesBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'original':
          result.original.replace(serializers.deserialize(value,
              specifiedType: const FullType(Original))! as Original);
          break;
        case 'downsizedStill':
          result.downsizedStill.replace(serializers.deserialize(value,
                  specifiedType: const FullType(DownsizedStill))!
              as DownsizedStill);
          break;
      }
    }

    return result.build();
  }
}

class _$Images extends Images {
  @override
  final Original? original;
  @override
  final DownsizedStill? downsizedStill;

  factory _$Images([void Function(ImagesBuilder)? updates]) =>
      (new ImagesBuilder()..update(updates)).build();

  _$Images._({this.original, this.downsizedStill}) : super._();

  @override
  Images rebuild(void Function(ImagesBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ImagesBuilder toBuilder() => new ImagesBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Images &&
        original == other.original &&
        downsizedStill == other.downsizedStill;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, original.hashCode), downsizedStill.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Images')
          ..add('original', original)
          ..add('downsizedStill', downsizedStill))
        .toString();
  }
}

class ImagesBuilder implements Builder<Images, ImagesBuilder> {
  _$Images? _$v;

  OriginalBuilder? _original;
  OriginalBuilder get original => _$this._original ??= new OriginalBuilder();
  set original(OriginalBuilder? original) => _$this._original = original;

  DownsizedStillBuilder? _downsizedStill;
  DownsizedStillBuilder get downsizedStill =>
      _$this._downsizedStill ??= new DownsizedStillBuilder();
  set downsizedStill(DownsizedStillBuilder? downsizedStill) =>
      _$this._downsizedStill = downsizedStill;

  ImagesBuilder();

  ImagesBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _original = $v.original?.toBuilder();
      _downsizedStill = $v.downsizedStill?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Images other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Images;
  }

  @override
  void update(void Function(ImagesBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Images build() {
    _$Images _$result;
    try {
      _$result = _$v ??
          new _$Images._(
              original: _original?.build(),
              downsizedStill: _downsizedStill?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'original';
        _original?.build();
        _$failedField = 'downsizedStill';
        _downsizedStill?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Images', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
