import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:gif_trending/src/model/serializers.dart';
import 'package:gif_trending/src/model/trending/images.dart';

part 'gif.g.dart';

///
abstract class Gif implements Built<Gif, GifBuilder> {

  ///
  String get id;

  ///
  String get type;

  ///
  Images get images;

  
  Gif._();

  ///
  factory Gif([void Function(GifBuilder) updates]) = _$Gif;

  ///
  Map<String, dynamic> toJson() {
    return serializers.serializeWith(Gif.serializer, this)
        as Map<String, dynamic>;
  }

  ///
  static Gif? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(Gif.serializer, json);
  }

  ///
  static Serializer<Gif> get serializer => _$gifSerializer;
}
