import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:gif_trending/src/model/serializers.dart';

part 'original.g.dart';

abstract class Original implements Built<Original, OriginalBuilder> {

  ///
  String? get url;

  ///
  String? get width;

  ///
  String? get height;

  Original._();

  ///
  factory Original([void Function(OriginalBuilder) updates]) = _$Original;

  ///
  Map<String, dynamic> toJson() {
    return serializers.serializeWith(Original.serializer, this)
        as Map<String, dynamic>;
  }

  ///
  static Original? fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(Original.serializer, json);
  }

  ///
  static Serializer<Original> get serializer => _$originalSerializer;
}
