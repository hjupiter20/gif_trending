import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:gif_trending/src/model/trending/downsized_still.dart';
import 'package:gif_trending/src/model/trending/gif.dart';
import 'package:gif_trending/src/model/trending/images.dart';
import 'package:gif_trending/src/model/trending/original.dart';

part 'serializers.g.dart';

@SerializersFor([Gif, Images, Original, DownsizedStill])

///
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();

///
T? deserialize<T>(dynamic value) => serializers.deserializeWith<T>(
    serializers.serializerForType(T) as Serializer<T>, value);

///
BuiltList<T> deserializeListOf<T>(dynamic value) => BuiltList.from(value
    .map((dynamic value) => deserialize<T>(value))
    .toList(growable: false));
