import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:gif_trending/src/controller/home_controller.dart';
import 'package:gif_trending/util/size_config.dart';

/// home page
class HomePage extends GetView<HomeController> {
  /// construct for home page
  HomePage({Key? key}) : super(key: key);

  // final _debouncer = Debouncer(milliseconds: 500);
  Timer? _debounce;

  //static final HomeController _homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: Column(children: [_searchWidget(), _bodyWidget()]),
      ),
    );
  }

  Widget _searchWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: TextField(
        onChanged: (String value) async {
          if (_debounce?.isActive ?? false) _debounce!.cancel();
            _debounce = Timer(const Duration(milliseconds: 500), ()  async{
                await controller.search(value);
            });
        },
        decoration: const InputDecoration(
          label: Text(
            'Search by name',
          ),
          prefixIcon: Icon(
            Icons.search,
            color: Colors.blue,
          ),
        ),
      ),
    );
  }

  _bodyWidget() {
    return SizedBox(
        height: SizeConfig.safeBlockVertical * 85,
        width: SizeConfig.safeBlockHorizontal * 90,
        child: Obx(
          () => MasonryGridView.count(
            itemCount: controller.lstItems.length,
            crossAxisCount: 2,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            itemBuilder: (BuildContext context, int index) {
              return ImageTile(
                index: index,
                width: 100,
                height: 100,
              );
            },
          ),
        ));
  }
}

///
class ImageTile extends StatelessWidget {
  ///
  const ImageTile({
    Key? key,
    required this.index,
    required this.width,
    required this.height,
  }) : super(key: key);

  ///
  static HomeController homeController = Get.find();

  ///
  final int index;

  ///
  final int width;

  ///
  final int height;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          child: Image.network(
            homeController.lstItems[index].images.original!.url.toString(),
            width: double.infinity,
            fit: BoxFit.fitWidth,
            loadingBuilder: (BuildContext context, Widget child,
                      ImageChunkEvent? loadingProgress) {
                    if (loadingProgress == null) return child;
                    return Center(
                      child: CircularProgressIndicator(
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes!
                            : null,
                      ),
                    );
                  },
          ),
        ),
        /*Positioned(
            bottom: 0,
            right: 0,
            child: ElevatedButton(
              onPressed: () {},
              child: Icon(
                Icons.menu,
                color: Colors.white,
                size: 5,
              ),
              style: ElevatedButton.styleFrom(
                shape: CircleBorder(),
                padding: EdgeInsets.all(20),
                primary: Colors.blue, // <-- Button color
                onPrimary: Colors.red, // <-- Splash color
              ),
            ))*/
      ],
    );
  }
}
