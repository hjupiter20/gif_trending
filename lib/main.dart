import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gif_trending/src/view/home_page.dart';
import 'package:gif_trending/util/theme.dart';

void main() {
  runApp(const MyApp());
}

/// main class for app
class MyApp extends StatelessWidget {

  /// construct for main class
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: lightTheme,
      home: HomePage(),
    );
  }
}